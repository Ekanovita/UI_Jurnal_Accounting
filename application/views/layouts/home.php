<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Jurnal Accounting</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('public/plugins/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?= base_url('public/dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/dist/css/styles.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/dist/css/skins/_all-skins.min.css') ?>">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <a href="#" class="logo">
          <span class="logo-lg"><b>ACCOUNTING</b></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <section class="sidebar">
          <ul class="sidebar-menu">
            <li>
              <a href="#">
                <i class="fa fa-dashboard"></i><span> Dashboard</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-area-chart"></i><span> Laporan</span> 
              </a>
            </li>
            <li class="v-nav-divider">
            </li>
            <li>
              <a href="#">
                <i class="fa fa-university"></i> <span>Khas & Bank</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-money"></i><span>Penjualan</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-credit-card"></i>
                <span>Pembelian</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-usd"></i> <span>Biaya</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-users"></i> <span>Pelanggan</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-truck"></i> <span>Suplier</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-cube"></i> <span>Produk</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-book"></i> <span>Daftar Akun</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-list"></i> <span>Daftar Lainnya</span>
              </a>
            <li>
              <a href="#">
                <i class="fa fa-cogs"></i> <span>Setting</span>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-sign-out"></i>Keluar<span></span>
              </a>
            </li>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->
      <div class="content-wrapper">
        <section>
          <div id='main-content'>
            <header class='page-heading border-bottom white-bg'>
              <div class='row'>
                <div class='col-md-12 col-sm-12 col-xs-12'>
                  <div class='trial'>
                    Masa percobaan berakhir dalam  14  hari
                  </div>
                </div>
              </div>
              <div class='row'>
              <div class='col-md-8 col-sm-12 col-xs-12'>
                <div class='page-title-heading'>
                  <h2 style='float:left;'>
                    Gambar Ringkasan Bisnis
                  </h2>
                </div>
                <div class='dropdown' style='float:left; margin-top:9px; margin-left:11px;'>
                  <button aria-expanded='false' aria-haspopup='true' data-toggle='dropdown' id='dLabel' type='button'>
                    month
                      <span class='fa fa-chevron-down' style='margin-left: 9px'></span>
                  </button>
                  <ul aria-labelledby='dLabel' class='dropdown-menu size-menu-open' style='padding: 5px 15px;'>
                    <li>
                      <form class='filter_class' id='dashboard_date_filter'>
                        <div class='checkboxgroup'>
                          <input end-data-value='11/04/2016' id='inlineRadio1' name='inlineRadioOptions' start-data-value='11/04/2016' type='radio' value='day'>
                            <label>
                              hari ini
                            </label>
                        </div>
                        <div class='checkboxgroup'>
                          <input end-data-value='17/04/2016' id='inlineRadio2' name='inlineRadioOptions' start-data-value='11/04/2016' type='radio' value='week'>
                            <label>
                              minggu ini
                            </label>
                        </div>
                        <div class='checkboxgroup'>
                          <input end-data-value='30/04/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/04/2016' type='radio' value='month'>
                            <label>
                              bulan ini
                            </label>
                        </div>
                        <div class='checkboxgroup'>
                          <input end-data-value='30/06/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/04/2016' type='radio' value='quarter'>
                            <label>
                              kuartal ini
                            </label>
                        </div>
                        <div class='checkboxgroup'>
                          <input end-data-value='31/12/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/01/2016' type='radio' value='year'>
                            <label>
                              tahun ini
                            </label>
                        </div>
                        <div class='checkboxgroup'>
                          <input end-data-value='30/04/2016' id='inlineRadio3' name='inlineRadioOptions' start-data-value='01/04/2016' type='radio' value='custom'>
                            <label>
                              sesuaikan
                            </label>
                        </div>
                      </form>
                    </li>
                  </ul>
                </div>
              </div>
              <div class='col-md-4 col-sm-12 col-xs-12'>
                <div class='insert-logo-link'>
                  <a href="/company/setting">Upload Company Logo</a>
                </div>
                </div>
              </div>
            </header>
          </div>
        </section>
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              <h3>Penjualan</h3><small>(tahun ini)</small>
                <div class="box">
                  <div class="box-header with-border">
                    <h2 class="box-title">Belum dibayar&nbsp;</h2><small>(dalam IDR)</small>
                  </div>
                  <div class="box-body">
                    <h3>Rp 0,00</h3>
                  </div><!-- /.box-body --><!-- /.box-footer-->
                </div><!-- /.box -->
            </div>
          </div>
        </section>
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <strong>Copyright &copy; 2014-2015 <a href="#">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>

    <!-- jQuery 2.1.4 -->
    <script src="<?= base_url('public/plugins/jQuery/jQuery-2.2.0.min.js') ?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?= base_url('public/bootstrap/js/bootstrap.min.js') ?>"></script>
    <!-- SlimScroll -->
    <script src="<?= base_url('public/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
    <!-- FastClick -->
    <script src="<?= base_url('public/plugins/fastclick/fastclick.min.js') ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('public/dist/js/app.min.js') ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url('publc/dist/js/demo.js') ?>"></script>
  </body>
</html>
